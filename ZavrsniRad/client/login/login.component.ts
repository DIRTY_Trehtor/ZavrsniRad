import { Component } from "@angular/core";
import {Router, Routes} from '@angular/router';
import { Client } from "../Client/Client";
import {CookieService} from 'angular2-cookie/core';

import * as io from "socket.io-client"
import * as global from "../Global/Global";

@Component({
    moduleId: module.id,
    selector: "login",
    templateUrl: "Login.html"
})

export class LoginComponent {

    userName: string = undefined;
    age: number = undefined;
    info: boolean = false;

    constructor(private _router: Router, private _cookieService: CookieService){
        
    }

    zapocni(){
        let ponovno = false;
        if(this.userName == undefined){
            ponovno = true;
        }
        else
            if(this.userName === ""){
                ponovno = true;
            }
        
        if(this.age == undefined){
            ponovno = true;
        }

        if(ponovno){
            alert("Popunjavanje svih kućica jest nužno za nastavak.");
            return;
        }
        if(this.age > 14){
            return;

        }
        let refference = this;
        let myCredentials: Client;

        
        //global.setSocket(io({query: "userName=" + this.userName+"&age="+ this.age}));
        global.setSocket(io.connect("http://localhost:3000", {'forceNew': true}));
        global.socket.emit("korisnikPodaci", this.userName, this.age);
        //global.setSocket(new io.Socket());
        //global.socket.connect();    
        global.socket.on("userCredentials", function(data){
            myCredentials = data;
            //COOKIES SPREMANJE
            refference._cookieService.put(global.myName, myCredentials.name);
            refference._cookieService.put(global.myAge, myCredentials.age.toString());
            refference._cookieService.put(global.myId, myCredentials.id);
            
            //na HOME screen
            refference._router.navigate(['/home']);
        });
    }
}