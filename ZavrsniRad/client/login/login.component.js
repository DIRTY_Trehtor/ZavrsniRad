"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var core_2 = require("angular2-cookie/core");
var io = require("socket.io-client");
var global = require("../Global/Global");
var LoginComponent = (function () {
    function LoginComponent(_router, _cookieService) {
        this._router = _router;
        this._cookieService = _cookieService;
        this.userName = undefined;
        this.age = undefined;
        this.info = false;
    }
    LoginComponent.prototype.zapocni = function () {
        var ponovno = false;
        if (this.userName == undefined) {
            ponovno = true;
        }
        else if (this.userName === "") {
            ponovno = true;
        }
        if (this.age == undefined) {
            ponovno = true;
        }
        if (ponovno) {
            alert("Popunjavanje svih kućica jest nužno za nastavak.");
            return;
        }
        if (this.age > 14) {
            return;
        }
        var refference = this;
        var myCredentials;
        //global.setSocket(io({query: "userName=" + this.userName+"&age="+ this.age}));
        global.setSocket(io.connect("http://localhost:3000", { 'forceNew': true }));
        global.socket.emit("korisnikPodaci", this.userName, this.age);
        //global.setSocket(new io.Socket());
        //global.socket.connect();    
        global.socket.on("userCredentials", function (data) {
            myCredentials = data;
            //COOKIES SPREMANJE
            refference._cookieService.put(global.myName, myCredentials.name);
            refference._cookieService.put(global.myAge, myCredentials.age.toString());
            refference._cookieService.put(global.myId, myCredentials.id);
            //na HOME screen
            refference._router.navigate(['/home']);
        });
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "login",
        templateUrl: "Login.html"
    }),
    __metadata("design:paramtypes", [router_1.Router, core_2.CookieService])
], LoginComponent);
exports.LoginComponent = LoginComponent;
