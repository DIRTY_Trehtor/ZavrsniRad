import { Component } from "@angular/core";
import {Router, Routes} from '@angular/router';

import * as io from "socket.io-client"
import * as global from "../../Global/Global";


@Component({
    moduleId: module.id,
    selector: "homeNavigation",
    templateUrl: "HomeNavigation.html"
})
export class HomeNavigationComponent{

    zakljucano:boolean = false;

    constructor(){

        let reff = this;

        global.socket.on("zakljucaj", function(){
            reff.zakljucano = true;
        });
        global.socket.on("otkljucaj", function(){
            reff.zakljucano = false;
        });

    }
}