import { Component } from "@angular/core";
import {Router, Routes} from '@angular/router';
import { Client } from "../Client/Client";
import { ZahtjevDvojke } from "../dvojke/ZahtjevDvojke/ZahtjevDvojke";
import { ZahtjevTrojke } from "../trojke/ZahtjevTrojke/ZahtjevTrojke";

import {CookieService} from 'angular2-cookie/core';

import * as io from "socket.io-client"
import * as global from "../Global/Global";

@Component({
    moduleId: module.id,
    selector: "home",
    templateUrl: "Home.html"
})
export class HomeComponent{

    usersOnline: Client[] = [];
    myRequests: ZahtjevDvojke[];
    myRequestsTrojke: ZahtjevTrojke[];
    thisUser: Client;
    zahtjevTrojke: ZahtjevTrojke;

    pozicija: number;

    zahtjevPotdvrdjen: boolean = false;
    zahtjevTrojkeUklonjen: boolean = false;

    //bitno da ne prekinemo učenje
    zakljucano: boolean = false;

    //poruke
    porukeSvima: String[] = [];
    porukeUcenje: String[] = [];
    chatText: string;
    imeSobe: string;

    constructor(private _cookieService: CookieService, private _router: Router){
        let reff = this;

        //PODACI O MENI
        let name: string = this._cookieService.get(global.myName);
        let age: number = Number(this._cookieService.get(global.myAge));
        let id: string = this._cookieService.get(global.myId);
        this.thisUser = new Client(name, age,id, 0);


       global.socket.emit("getMyRequestsTrojke", id);
       global.socket.on("requestsTrojke", function(zahtjevi){
           reff.myRequestsTrojke = zahtjevi;
       });
       global.socket.on("updateZahtjevTrojke", function(zahtjev){
            reff.zahtjevTrojke = zahtjev;

        });
        global.socket.on("zahtjevTrojkeUklonjen", function(){

            if(reff.zahtjevTrojke != undefined)
                reff.zahtjevTrojkeUklonjen = true;
        });



        global.socket.emit("getMyRequests", id);
       global.socket.on("requestsDvojke", function(zahtjevi){
           
           // alert("Primam dvojke. Broj: " + zahtjevi.length);

            reff.myRequests = [];
            reff.myRequests = zahtjevi;
            
       });
        /* DOBRO RADI */
        global.socket.emit("getUsersOnline");
        global.socket.on("broadcastUsersOnline", function(usersOnline){
            reff.usersOnline = usersOnline;
        });
        global.socket.on("userConnected", function(user){
            reff.usersOnline.push(user);
        });
        global.socket.on("userDisconnected", function(user){
            let index;
            for(let el of reff.usersOnline){
                if(user.id === el.id){
                    break;
                }
                index ++;
            }
            reff.usersOnline.splice(index, 1);
        });
        global.socket.on("imeSobe", function(imeSobe, vrsta){ //vrsta: 2-dvojka 3-trojka

            //alert("Ime sobe");

            reff._cookieService.put(global.myRoom, imeSobe);
            reff._cookieService.put(global.myVrsta, vrsta);
            if(vrsta == 2){
                reff._router.navigate(['/home/zio1']);
            }
            reff.zahtjevPotdvrdjen = true;
        });

        global.socket.on("zakljucaj", function(imeSobe){
            //alert("Zakljucava se");
            reff.zakljucano = true;
            reff.imeSobe = imeSobe;
        });
        global.socket.on("otkljucaj", function(){
            reff.zakljucano = false;
            reff.chatText = "";
            reff.porukeUcenje = [];
        });
        global.socket.on("porukaSvima", function(poruka){
            reff.porukeSvima.push(poruka);
        });
        global.socket.on("porukeUcenje", function(poruka){
            reff.porukeUcenje.push(poruka);
        });
        
    }

    zapocni(){
            this.zahtjevPotdvrdjen = false;
            this.zahtjevTrojke = undefined;
            this._router.navigate(['/home/zio1']);
    }

    potvrdiDvojku(req : ZahtjevDvojke){
        let reff = this;
        global.socket.emit("potvrdaZahtjeva", req);
        global.socket.emit("ponistiZahtjevRandomDvojke", this.thisUser.id);//za svaki slućaj ako smo u nekom zahtjevi...
        global.socket.emit("ponistiZahtjevRandomTrojke", this.thisUser.id);
    }

    potvrdiTrojku(req: ZahtjevTrojke){
        let reff = this;
        reff.zahtjevTrojkeUklonjen = false;
        reff.zahtjevPotdvrdjen = false;

        if(this.zahtjevTrojke == undefined){
            this.zahtjevTrojke = req;
        }

        if(req.prima1.id === this.thisUser.id){
            this.zahtjevTrojke.prima1.status = true;
            
            global.socket.emit("potvrdaZahtjevaTrojke", this.zahtjevTrojke, 1);
        }
        else if(req.prima2.id === this.thisUser.id){
            this.zahtjevTrojke.prima2.status = true;

            global.socket.emit("potvrdaZahtjevaTrojke", this.zahtjevTrojke, 2);
        }
        
    }
    ponistiPotvrduTrojka(){

        if(this.pozicija == 1){
            this.zahtjevTrojke.prima1.status = false;
        }
        else{
            this.zahtjevTrojke.prima2.status = false;
        }

        // NAPOMENA: iako se zove potvrda, radi i ukoliko želimo POVUĆI POTVRDU
        global.socket.emit("potvrdaZahtjevaTrojke", this.zahtjevTrojke, this.pozicija);
        this.zahtjevTrojke = undefined; //BITNO!!! Da se može pridodijeliti novi zahtjev za pračenje
    }
    saljiPoruku(){
        if(this.chatText != undefined){
            if(this.chatText !== ""){
                let name = this._cookieService.get(global.myName);
                let poruka ="[ "+ name + " ]: " + this.chatText;

                if(!this.zakljucano){
                    this.porukeSvima.push(poruka);
                    global.socket.emit("poruka", poruka);
                }
                else{
                    this.porukeUcenje.push(poruka);
                    global.socket.emit("porukaUcenje", this.imeSobe, poruka);
                }

                
                this.chatText = "";
            }
        }
    }
}