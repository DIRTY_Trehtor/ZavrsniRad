"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Client_1 = require("../Client/Client");
var core_2 = require("angular2-cookie/core");
var global = require("../Global/Global");
var HomeComponent = (function () {
    function HomeComponent(_cookieService, _router) {
        this._cookieService = _cookieService;
        this._router = _router;
        this.usersOnline = [];
        this.zahtjevPotdvrdjen = false;
        this.zahtjevTrojkeUklonjen = false;
        //bitno da ne prekinemo učenje
        this.zakljucano = false;
        //poruke
        this.porukeSvima = [];
        this.porukeUcenje = [];
        var reff = this;
        //PODACI O MENI
        var name = this._cookieService.get(global.myName);
        var age = Number(this._cookieService.get(global.myAge));
        var id = this._cookieService.get(global.myId);
        this.thisUser = new Client_1.Client(name, age, id, 0);
        global.socket.emit("getMyRequestsTrojke", id);
        global.socket.on("requestsTrojke", function (zahtjevi) {
            reff.myRequestsTrojke = zahtjevi;
        });
        global.socket.on("updateZahtjevTrojke", function (zahtjev) {
            reff.zahtjevTrojke = zahtjev;
        });
        global.socket.on("zahtjevTrojkeUklonjen", function () {
            if (reff.zahtjevTrojke != undefined)
                reff.zahtjevTrojkeUklonjen = true;
        });
        global.socket.emit("getMyRequests", id);
        global.socket.on("requestsDvojke", function (zahtjevi) {
            // alert("Primam dvojke. Broj: " + zahtjevi.length);
            reff.myRequests = [];
            reff.myRequests = zahtjevi;
        });
        /* DOBRO RADI */
        global.socket.emit("getUsersOnline");
        global.socket.on("broadcastUsersOnline", function (usersOnline) {
            reff.usersOnline = usersOnline;
        });
        global.socket.on("userConnected", function (user) {
            reff.usersOnline.push(user);
        });
        global.socket.on("userDisconnected", function (user) {
            var index;
            for (var _i = 0, _a = reff.usersOnline; _i < _a.length; _i++) {
                var el = _a[_i];
                if (user.id === el.id) {
                    break;
                }
                index++;
            }
            reff.usersOnline.splice(index, 1);
        });
        global.socket.on("imeSobe", function (imeSobe, vrsta) {
            //alert("Ime sobe");
            reff._cookieService.put(global.myRoom, imeSobe);
            reff._cookieService.put(global.myVrsta, vrsta);
            if (vrsta == 2) {
                reff._router.navigate(['/home/zio1']);
            }
            reff.zahtjevPotdvrdjen = true;
        });
        global.socket.on("zakljucaj", function (imeSobe) {
            //alert("Zakljucava se");
            reff.zakljucano = true;
            reff.imeSobe = imeSobe;
        });
        global.socket.on("otkljucaj", function () {
            reff.zakljucano = false;
            reff.chatText = "";
            reff.porukeUcenje = [];
        });
        global.socket.on("porukaSvima", function (poruka) {
            reff.porukeSvima.push(poruka);
        });
        global.socket.on("porukeUcenje", function (poruka) {
            reff.porukeUcenje.push(poruka);
        });
    }
    HomeComponent.prototype.zapocni = function () {
        this.zahtjevPotdvrdjen = false;
        this.zahtjevTrojke = undefined;
        this._router.navigate(['/home/zio1']);
    };
    HomeComponent.prototype.potvrdiDvojku = function (req) {
        var reff = this;
        global.socket.emit("potvrdaZahtjeva", req);
        global.socket.emit("ponistiZahtjevRandomDvojke", this.thisUser.id); //za svaki slućaj ako smo u nekom zahtjevi...
        global.socket.emit("ponistiZahtjevRandomTrojke", this.thisUser.id);
    };
    HomeComponent.prototype.potvrdiTrojku = function (req) {
        var reff = this;
        reff.zahtjevTrojkeUklonjen = false;
        reff.zahtjevPotdvrdjen = false;
        if (this.zahtjevTrojke == undefined) {
            this.zahtjevTrojke = req;
        }
        if (req.prima1.id === this.thisUser.id) {
            this.zahtjevTrojke.prima1.status = true;
            global.socket.emit("potvrdaZahtjevaTrojke", this.zahtjevTrojke, 1);
        }
        else if (req.prima2.id === this.thisUser.id) {
            this.zahtjevTrojke.prima2.status = true;
            global.socket.emit("potvrdaZahtjevaTrojke", this.zahtjevTrojke, 2);
        }
    };
    HomeComponent.prototype.ponistiPotvrduTrojka = function () {
        if (this.pozicija == 1) {
            this.zahtjevTrojke.prima1.status = false;
        }
        else {
            this.zahtjevTrojke.prima2.status = false;
        }
        // NAPOMENA: iako se zove potvrda, radi i ukoliko želimo POVUĆI POTVRDU
        global.socket.emit("potvrdaZahtjevaTrojke", this.zahtjevTrojke, this.pozicija);
        this.zahtjevTrojke = undefined; //BITNO!!! Da se može pridodijeliti novi zahtjev za pračenje
    };
    HomeComponent.prototype.saljiPoruku = function () {
        if (this.chatText != undefined) {
            if (this.chatText !== "") {
                var name_1 = this._cookieService.get(global.myName);
                var poruka = "[ " + name_1 + " ]: " + this.chatText;
                if (!this.zakljucano) {
                    this.porukeSvima.push(poruka);
                    global.socket.emit("poruka", poruka);
                }
                else {
                    this.porukeUcenje.push(poruka);
                    global.socket.emit("porukaUcenje", this.imeSobe, poruka);
                }
                this.chatText = "";
            }
        }
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "home",
        templateUrl: "Home.html"
    }),
    __metadata("design:paramtypes", [core_2.CookieService, router_1.Router])
], HomeComponent);
exports.HomeComponent = HomeComponent;
