"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Client = (function () {
    function Client(name, age, id, pravo) {
        this.name = name;
        this.age = age;
        this.id = id;
        this.pravo = pravo;
        this.odabran = false;
        this.status = false;
    }
    return Client;
}());
exports.Client = Client;
