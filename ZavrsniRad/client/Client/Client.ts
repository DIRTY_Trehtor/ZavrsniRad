export class Client{
    name: string;
    age: number;
    id: string;
    pravo: number;
    status: boolean;

    odabran: boolean;

    constructor(name: string, age: number, id: string, pravo: number) {
        this.name = name;
        this.age = age;
        this.id = id;
        this.pravo = pravo;

        this.odabran = false;
        this.status = false;
    }
}