import { NgModule }      from "@angular/core";
import {Router, Routes} from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule }   from "@angular/forms";
import { RouterModule } from "@angular/router";

import { CookieService } from 'angular2-cookie/core';

import {HashLocationStrategy, LocationStrategy} from '@angular/common';

//komponente modula
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import {NavbarComponent } from "./navbar/navbar.component";
import { LoginNavbarComponent } from "./loginNavbar/loginNavbar.component";
import { HomeComponent } from "./home/home.component";
    import { HomeInfoComponent } from "./homeInfo/homeInfo.component";
    import { DvojkeIndexComponent } from "./dvojke/dvojkeIndex.component";
        import { RandomDvojkeComponent } from "./dvojke/random/random.component";
        import { FrendDvojkeComponent } from "./dvojke/frend/frend.component";
        import { InfoDvojkeComponent } from "./dvojke/info/infoDvojke.component";
    import { TrojkeIndexComponent } from "./trojke/trojkeIndex.component";
        import { InfoTrojkeComponent } from "./trojke/info/infoTrojke.component";
        import { FrendTrojkeComponent } from "./trojke/frend/frend.component";
        import { RandomTrojkeComponent } from "./trojke/random/random.component";
    import { Zio1Component } from "./Tecajevi/ZbrajanjeIOduzimanje/zio1.component";
import { HomeNavigationComponent } from "./home/homeNavigation/homeNavigation.component";


//routanje
import { appRoutes } from "./app.routing";

@NgModule({
    imports:        [BrowserModule, FormsModule, RouterModule, RouterModule.forRoot(appRoutes)],
    declarations:   [AppComponent,
                     LoginComponent, NavbarComponent, LoginNavbarComponent,
                     HomeComponent, HomeInfoComponent, HomeNavigationComponent, Zio1Component,
                        DvojkeIndexComponent, RandomDvojkeComponent, FrendDvojkeComponent, InfoDvojkeComponent,
                        TrojkeIndexComponent, InfoTrojkeComponent, FrendTrojkeComponent, RandomTrojkeComponent,
                     ],
                     //pazi na ovo kad stavljas na server
                     //, CookieService
    providers:      [CookieService ,{provide: LocationStrategy, useClass: HashLocationStrategy}],
    bootstrap:      [AppComponent]
})
export class AppModule{}