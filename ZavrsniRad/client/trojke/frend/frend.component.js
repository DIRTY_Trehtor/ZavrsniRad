"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Client_1 = require("../../Client/Client");
var ZahtjevTrojke_1 = require("../ZahtjevTrojke/ZahtjevTrojke");
var core_2 = require("angular2-cookie/core");
var global = require("../../Global/Global");
var FrendTrojkeComponent = (function () {
    function FrendTrojkeComponent(_router, _cookieService) {
        this._router = _router;
        this._cookieService = _cookieService;
        this.brojOdabranih = 0;
        this.odabrani = [];
        this.zahtjevPotdvrdjen = false;
        var reff = this;
        var id = reff._cookieService.get(global.myId);
        var name = reff._cookieService.get(global.myName);
        global.socket.emit("getUsersOnline");
        global.socket.on("broadcastUsersOnline", function (usersOnline) {
            var pom = usersOnline;
            reff.usersOnline = [];
            for (var _i = 0, pom_1 = pom; _i < pom_1.length; _i++) {
                var usr = pom_1[_i];
                usr.odabran = false; //resetiramo da mozemo nanovo birati
                usr.status = false;
                if (usr.id !== id) {
                    reff.usersOnline.push(usr);
                }
            }
        });
        global.socket.on("userConnected", function (user) {
            reff.usersOnline.push(user);
        });
        global.socket.on("userDisconnected", function (user) {
            var index;
            var i = 0;
            for (var _i = 0, _a = reff.odabrani; _i < _a.length; _i++) {
                var el = _a[_i];
                if (user.id === el.id) {
                    index = i;
                    break;
                }
                i++;
            }
            if (index != undefined) {
                reff.odabrani.splice(index, 1);
            }
            i = 0;
            index = undefined;
            for (var _b = 0, _c = reff.usersOnline; _b < _c.length; _b++) {
                var el = _c[_b];
                if (user.id === el.id) {
                    index = i;
                    break;
                }
                i++;
            }
            reff.usersOnline.splice(index, 1);
        });
        global.socket.on("updateZahtjevTrojke", function (zahtjev) {
            reff.zahtjev = zahtjev;
        });
        global.socket.on("imeSobe", function (imeSobe, vrsta) {
            reff._cookieService.put(global.myRoom, imeSobe);
            reff._cookieService.put(global.myVrsta, vrsta);
            reff.zahtjevPotdvrdjen = true;
            global.socket.emit("ponistiZahtjevTrojke", id);
        });
    }
    FrendTrojkeComponent.prototype.zapocni = function () {
        this._router.navigate(['/home/zio1']);
    };
    FrendTrojkeComponent.prototype.odaberi = function (user) {
        if (user.odabran) {
            user.odabran = false;
            //brisemo ga s liste odabranih
            var index = void 0;
            var i = 0;
            for (var _i = 0, _a = this.odabrani; _i < _a.length; _i++) {
                var el = _a[_i];
                if (el.id === user.id) {
                    index = i;
                }
                i++;
            }
            this.odabrani.splice(index, 1);
            this.brojOdabranih--;
        }
        else {
            if (this.brojOdabranih == 2) {
                return;
            }
            user.odabran = true;
            this.odabrani.push(user);
            this.brojOdabranih++;
        }
    };
    FrendTrojkeComponent.prototype.posaljiZahtjev = function () {
        var name = this._cookieService.get(global.myName);
        var age = Number(this._cookieService.get(global.myAge));
        var id = this._cookieService.get(global.myId);
        var me = new Client_1.Client(name, age, id, 0);
        this.zahtjev = new ZahtjevTrojke_1.ZahtjevTrojke(me, this.odabrani[0], this.odabrani[1]);
        global.socket.emit("zahtjevTrojke", this.zahtjev);
    };
    FrendTrojkeComponent.prototype.ponistiZahtjev = function () {
        var id = this._cookieService.get(global.myId);
        global.socket.emit("ponistiZahtjevTrojke", id);
    };
    return FrendTrojkeComponent;
}());
FrendTrojkeComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "frendTrojke",
        templateUrl: "Frend.html"
    }),
    __metadata("design:paramtypes", [router_1.Router, core_2.CookieService])
], FrendTrojkeComponent);
exports.FrendTrojkeComponent = FrendTrojkeComponent;
