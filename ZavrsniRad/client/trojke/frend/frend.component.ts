import { Component } from "@angular/core";
import {Router, Routes} from '@angular/router';
import { Client } from "../../Client/Client";
import { ZahtjevTrojke } from "../ZahtjevTrojke/ZahtjevTrojke";

import {CookieService} from 'angular2-cookie/core';

import * as io from "socket.io-client"
import * as global from "../../Global/Global";

@Component({
    moduleId: module.id,
    selector: "frendTrojke",
    templateUrl: "Frend.html"
})
export class FrendTrojkeComponent{

    usersOnline: Client[];
    brojOdabranih: number = 0;
    odabrani: Client[] = [];
    zahtjev: ZahtjevTrojke;
    zahtjevPotdvrdjen: boolean = false;

    constructor(private _router: Router, private _cookieService: CookieService){
        let reff = this;
        let id: string = reff._cookieService.get(global.myId);
        let name: string = reff._cookieService.get(global.myName);

        global.socket.emit("getUsersOnline");
        global.socket.on("broadcastUsersOnline", function(usersOnline){
            let pom: Client[] = usersOnline;
            reff.usersOnline = [];
            
            for(let usr of pom){
                usr.odabran = false;    //resetiramo da mozemo nanovo birati
                usr.status = false;
                if(usr.id !== id){
                    reff.usersOnline.push(usr);
                }
            }
        });
        global.socket.on("userConnected", function(user){
            reff.usersOnline.push(user);
        });
        global.socket.on("userDisconnected", function(user){
            
            
            let index;
            let i = 0;
            for(let el of reff.odabrani){
                if(user.id === el.id){
                    index = i;
                    break;
                }
                i++;
            }
            if(index != undefined){
                reff.odabrani.splice(index, 1);
            }


            i = 0;
            index = undefined;
            for(let el of reff.usersOnline){
                if(user.id === el.id){
                    index = i;
                    break;
                }
                i++;
            }
            reff.usersOnline.splice(index, 1);
        });
        global.socket.on("updateZahtjevTrojke", function(zahtjev){
            reff.zahtjev = zahtjev;
        });
        global.socket.on("imeSobe", function(imeSobe , vrsta){
            reff._cookieService.put(global.myRoom, imeSobe);
            reff._cookieService.put(global.myVrsta, vrsta);
            reff.zahtjevPotdvrdjen = true;
            global.socket.emit("ponistiZahtjevTrojke", id);
        });
    }

    zapocni(){
        this._router.navigate(['/home/zio1']);
    }

    odaberi(user: Client){

        if(user.odabran){
            user.odabran = false;
            //brisemo ga s liste odabranih
            let index; let i = 0;
            for(let el of this.odabrani){
                if(el.id === user.id){
                    index = i;
                }
                i++;
            }
            this.odabrani.splice(index, 1);
            this.brojOdabranih--;
        }
        else{
            if(this.brojOdabranih == 2){
                return;
            }
            user.odabran = true;
            this.odabrani.push(user);
            this.brojOdabranih ++;
        }
        
    }
    posaljiZahtjev(){

        let name: string = this._cookieService.get(global.myName);
        let age: number = Number(this._cookieService.get(global.myAge));
        let id: string =this._cookieService.get(global.myId);
        let me = new Client(name, age, id, 0);

        this.zahtjev = new ZahtjevTrojke(me, this.odabrani[0], this.odabrani[1]);

        global.socket.emit("zahtjevTrojke", this.zahtjev);
    }
    ponistiZahtjev(){
        let id: string = this._cookieService.get(global.myId);
        global.socket.emit("ponistiZahtjevTrojke", id);
    }
}