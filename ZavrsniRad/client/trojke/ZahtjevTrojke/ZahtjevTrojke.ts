import { Client } from "../../Client/Client";

export class ZahtjevTrojke{

    daje: Client;
    prima1: Client;
    prima2: Client;

    constructor(daje: Client, prima1: Client, prima2: Client){
        this.daje = daje;
        this.prima1 = prima1;
        this.prima2 = prima2;
    }
}