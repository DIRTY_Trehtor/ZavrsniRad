"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ZahtjevTrojke = (function () {
    function ZahtjevTrojke(daje, prima1, prima2) {
        this.daje = daje;
        this.prima1 = prima1;
        this.prima2 = prima2;
    }
    return ZahtjevTrojke;
}());
exports.ZahtjevTrojke = ZahtjevTrojke;
