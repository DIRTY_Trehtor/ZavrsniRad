import { Component } from "@angular/core";
import {Router, Routes} from '@angular/router';
import { Client } from "../../Client/Client";

import {CookieService} from 'angular2-cookie/core';

import * as io from "socket.io-client"
import * as global from "../../Global/Global";

@Component({
    moduleId: module.id,
    selector: "randomTrojke",
    templateUrl: "Random.html"
})
export class RandomTrojkeComponent{

    thisUser: Client;
    cekanje: boolean = false;

    constructor(private _cookieService: CookieService, private _router: Router){
        
        let name = this._cookieService.get(global.myName);
        let age = Number(this._cookieService.get(global.myAge));
        let id = this._cookieService.get(global.myId);

        this.thisUser = new Client(name, age, id, 0);

        let reff = this;
        global.socket.on("imeSobe", function(imeSobe, vrsta){
            reff._cookieService.put(global.myRoom, imeSobe);
            reff._cookieService.put(global.myVrsta, vrsta);
            //navigacija
            reff._router.navigate(['/home/zio1']);

        });
    }

    zahtjevZaRandom(){
        global.socket.emit("zahtjevRandomTrojke", this.thisUser);
        this.cekanje = true;
    }
    ponistiZahtjev(){
        global.socket.emit("ponistiZahtjevRandomTrojke", this.thisUser.id);
        this.cekanje = false;
    }

}