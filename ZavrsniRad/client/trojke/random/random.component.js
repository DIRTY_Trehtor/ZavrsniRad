"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Client_1 = require("../../Client/Client");
var core_2 = require("angular2-cookie/core");
var global = require("../../Global/Global");
var RandomTrojkeComponent = (function () {
    function RandomTrojkeComponent(_cookieService, _router) {
        this._cookieService = _cookieService;
        this._router = _router;
        this.cekanje = false;
        var name = this._cookieService.get(global.myName);
        var age = Number(this._cookieService.get(global.myAge));
        var id = this._cookieService.get(global.myId);
        this.thisUser = new Client_1.Client(name, age, id, 0);
        var reff = this;
        global.socket.on("imeSobe", function (imeSobe, vrsta) {
            reff._cookieService.put(global.myRoom, imeSobe);
            reff._cookieService.put(global.myVrsta, vrsta);
            //navigacija
            reff._router.navigate(['/home/zio1']);
        });
    }
    RandomTrojkeComponent.prototype.zahtjevZaRandom = function () {
        global.socket.emit("zahtjevRandomTrojke", this.thisUser);
        this.cekanje = true;
    };
    RandomTrojkeComponent.prototype.ponistiZahtjev = function () {
        global.socket.emit("ponistiZahtjevRandomTrojke", this.thisUser.id);
        this.cekanje = false;
    };
    return RandomTrojkeComponent;
}());
RandomTrojkeComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "randomTrojke",
        templateUrl: "Random.html"
    }),
    __metadata("design:paramtypes", [core_2.CookieService, router_1.Router])
], RandomTrojkeComponent);
exports.RandomTrojkeComponent = RandomTrojkeComponent;
