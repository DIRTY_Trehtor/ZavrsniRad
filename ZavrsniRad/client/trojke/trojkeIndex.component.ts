import { Component } from "@angular/core";
import {Router, Routes} from '@angular/router';

import * as io from "socket.io-client"
import * as global from "../Global/Global";

@Component({
    moduleId: module.id,
    selector: "trojkeIndex",
    templateUrl: "TrojkeIndex.html"
})
export class TrojkeIndexComponent{

    brzo: boolean = false;

    constructor(private _router: Router){
        
    }

    brzoUcenje(){
        this.brzo = true;
        this._router.navigate(['/home/trojke/random']);
        
    }
    sPrijateljem(){
        this.brzo = false;
        this._router.navigate(['/home/trojke/frend']);

    }

}