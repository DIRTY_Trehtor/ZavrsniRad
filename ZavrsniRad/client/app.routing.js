"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//komponente
var login_component_1 = require("./login/login.component");
var home_component_1 = require("./home/home.component");
var homeInfo_component_1 = require("./homeInfo/homeInfo.component");
var dvojkeIndex_component_1 = require("./dvojke/dvojkeIndex.component");
var random_component_1 = require("./dvojke/random/random.component");
var frend_component_1 = require("./dvojke/frend/frend.component");
var infoDvojke_component_1 = require("./dvojke/info/infoDvojke.component");
var trojkeIndex_component_1 = require("./trojke/trojkeIndex.component");
var infoTrojke_component_1 = require("./trojke/info/infoTrojke.component");
var frend_component_2 = require("./trojke/frend/frend.component");
var random_component_2 = require("./trojke/random/random.component");
var zio1_component_1 = require("./Tecajevi/ZbrajanjeIOduzimanje/zio1.component");
exports.appRoutes = [
    { path: '', component: login_component_1.LoginComponent },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'home', component: home_component_1.HomeComponent, children: [
            { path: '', redirectTo: 'info' },
            { path: 'info', component: homeInfo_component_1.HomeInfoComponent },
            { path: 'dvojke', component: dvojkeIndex_component_1.DvojkeIndexComponent, children: [
                    { path: '', redirectTo: 'info' },
                    { path: 'info', component: infoDvojke_component_1.InfoDvojkeComponent },
                    { path: 'frend', component: frend_component_1.FrendDvojkeComponent },
                    { path: 'random', component: random_component_1.RandomDvojkeComponent },
                ] },
            { path: 'trojke', component: trojkeIndex_component_1.TrojkeIndexComponent, children: [
                    { path: '', redirectTo: 'info' },
                    { path: 'info', component: infoTrojke_component_1.InfoTrojkeComponent },
                    { path: 'frend', component: frend_component_2.FrendTrojkeComponent },
                    { path: 'random', component: random_component_2.RandomTrojkeComponent },
                ] },
            { path: 'zio1', component: zio1_component_1.Zio1Component },
        ] },
];
