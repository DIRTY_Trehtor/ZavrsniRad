import { Component, OnInit } from "@angular/core";
import {Router, Routes} from '@angular/router';
import {CookieService} from 'angular2-cookie/core';

import * as global from "../../Global/Global";
import { Client } from "../../Client/Client";
import { Trojka } from "../../trojke/Trojka/Trojka";
import { Dvojka } from "../../dvojke/Dvojka/Dvojka";

import {Observable} from 'rxjs/Rx';
import { Statistika } from "../Statistika/Stat";


import * as io from "socket.io-client"

@Component({
    moduleId: module.id,
    selector: "zio1",
    templateUrl: "Zio1.html"
})

export class Zio1Component {

    brojZadatka: number = 1;
    brojSvihZadataka: number = 3;

    statistika: Statistika[] = [];

    mojePravo: number;
    mojaSoba: string;
    soba: any;
    vrsta: number; //dvojka ili trojka
    operandi: number[] = [0 , 0 , 0, 0, 0, 0];

    mogucaPredaja: boolean = false;
    moguceOcijenjivanje: boolean = false;
    
    ocijena: boolean = undefined;
    evaluacija: boolean = undefined;

    zadajPritisnut: boolean = false;
    mogucaEvaluacija: boolean = false;
    mogucNastavak: boolean = false;

    chatText: string;
    poruke: string[] = [];

    ucenjePrekinuto: boolean = false;
    ucenjePrekinuo: string;

    ucenjeZavrseno: boolean = false;


    timer(t){
        if(t == 3){
            this._router.navigate(['/home']);
        }
    }

    constructor(private _cookieService : CookieService, private _router: Router){
        let reff = this;

        let mojId = this._cookieService.get(global.myId);
        this.mojaSoba = this._cookieService.get(global.myRoom);
        this.vrsta = Number(this._cookieService.get(global.myVrsta));

        global.socket.emit("dohvatiSobu", this.mojaSoba,this.vrsta);
        global.socket.on("soba", function(soba){
            
            //alert("Dobivamo sobu. Soba: " + soba);

            reff.soba = soba;
            if(soba == undefined){
                reff.ucenjePrekinuto = true;

                //alert("Ucenje prije prekinuto");

                let timer = Observable.timer(3000, 1000);
                //alert("Setam timer");
                timer.subscribe(t => {
                    reff.timer(t);
                });
                return;
            }

            reff.ucenjePrekinuto = false;

        });

        global.socket.emit("dohvatiSvojePravo", mojId, this.mojaSoba, reff.vrsta);
        global.socket.on("pravo", function(pravo){
            reff.mojePravo = Number(pravo);
        });
        global.socket.on("ucenjePrekinuto", function(clientName){
            //alert("Ucenje prekinuto");
            //alert("Prekinuto. Zavrseno: " + reff.ucenjeZavrseno);
            //alert("Zadatak: " + reff.brojZadatka);
            if(reff.brojZadatka == reff.brojSvihZadataka || reff.brojZadatka == (reff.brojSvihZadataka + 1)){
                return;
            }

            reff.ucenjePrekinuto = true;
            reff.ucenjePrekinuo = clientName;

            let timer = Observable.timer(3000, 1000);
            //alert("Setam timer");
                timer.subscribe(t => {
                    reff.timer(t);
                });
        })

        global.socket.on("zadatakZadan", function(operandi){
            reff.operandi = operandi;
            reff.mogucaPredaja = true;
        });
        global.socket.on("zadatakRjesen", function(operandi){
            reff.operandi = operandi;
            reff.moguceOcijenjivanje = true;
        });
        global.socket.on("zadatakOcijenjen", function(ocijena){

            //reff.statistika.push(new Statistika(reff.brojZadatka, ocijena));

            if(reff.vrsta == 3){
                reff.mogucaEvaluacija = true;
            }
            else{
                reff.mogucNastavak = true;
            }
            reff.ocijena = ocijena;
        });
        global.socket.on("zadatakEvaluiran", function(evaluacija){

            //reff.statistika[reff.brojZadatka-1].evaluacija = evaluacija;

            reff.mogucNastavak = true;
            reff.evaluacija = evaluacija;
        });
        global.socket.on("poruka", function(poruka){
            reff.poruke.push(poruka);
        });
        
    }

    zadatakZadan(){
        this.zadajPritisnut = true;
        global.socket.emit("zadatakZadan", this.mojaSoba, this.operandi);
    }
    zadatakRjesen(){
        this.mogucaPredaja = false;
        global.socket.emit("zadatakRjesen", this.mojaSoba, this.operandi);
    }
    zadatakOcijenjen(ishod){
        this.moguceOcijenjivanje = false;
        if(this.vrsta == 2){
        this.mogucNastavak = true;
        }
        global.socket.emit("zadatakOcijenjen", this.mojaSoba, ishod);
    }
    zadatakEvaluiran(ishod){
        this.mogucaEvaluacija = false;
        this.mogucNastavak = true;
        global.socket.emit("zadatakEvaluiran", this.mojaSoba, ishod);
    }

    nastavak(){
        //resetiraj sve

        this.mogucaPredaja = false;
        this.moguceOcijenjivanje = false;
        this.ocijena = undefined;
        this.evaluacija = undefined;
        this.zadajPritisnut = false;
        this.mogucNastavak = false;
        this.mogucaEvaluacija = false;

        this.operandi = [0 , 0 , 0, 0, 0, 0];

        this.brojZadatka ++;
        if(this.brojZadatka == (this.brojSvihZadataka + 1)){
            this.ucenjeZavrseno = true;
            global.socket.emit("napustanjeUcenja", this.mojaSoba, this.vrsta, undefined);
        }
    }

    zavrsi(){
        global.socket.emit("otkljucaj");
        this._router.navigate(['/home']);
    }

    saljiPoruku(){
        if(this.chatText != undefined){
            if(this.chatText !== ""){
                let name = this._cookieService.get(global.myName);
                let poruka ="[ "+ name + " ]: " + this.chatText;
                this.poruke.push(poruka);
                global.socket.emit("poruka", this.mojaSoba, poruka);
                this.chatText = "";
            }
        }
    }
    napusti(){
       
        let myName = this._cookieService.get(global.myName);

         global.socket.emit("napustanjeUcenja", this.mojaSoba, this.vrsta, myName);
         if(this.vrsta == 2){
            this._router.navigate(['/home/dvojke/info']);
         }
         else{
            this._router.navigate(['/home/trojke/info']);  
         }
         
         
    }

}