"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var core_2 = require("angular2-cookie/core");
var global = require("../../Global/Global");
var Rx_1 = require("rxjs/Rx");
var Zio1Component = (function () {
    function Zio1Component(_cookieService, _router) {
        this._cookieService = _cookieService;
        this._router = _router;
        this.brojZadatka = 1;
        this.brojSvihZadataka = 3;
        this.statistika = [];
        this.operandi = [0, 0, 0, 0, 0, 0];
        this.mogucaPredaja = false;
        this.moguceOcijenjivanje = false;
        this.ocijena = undefined;
        this.evaluacija = undefined;
        this.zadajPritisnut = false;
        this.mogucaEvaluacija = false;
        this.mogucNastavak = false;
        this.poruke = [];
        this.ucenjePrekinuto = false;
        this.ucenjeZavrseno = false;
        var reff = this;
        var mojId = this._cookieService.get(global.myId);
        this.mojaSoba = this._cookieService.get(global.myRoom);
        this.vrsta = Number(this._cookieService.get(global.myVrsta));
        global.socket.emit("dohvatiSobu", this.mojaSoba, this.vrsta);
        global.socket.on("soba", function (soba) {
            //alert("Dobivamo sobu. Soba: " + soba);
            reff.soba = soba;
            if (soba == undefined) {
                reff.ucenjePrekinuto = true;
                //alert("Ucenje prije prekinuto");
                var timer = Rx_1.Observable.timer(3000, 1000);
                //alert("Setam timer");
                timer.subscribe(function (t) {
                    reff.timer(t);
                });
                return;
            }
            reff.ucenjePrekinuto = false;
        });
        global.socket.emit("dohvatiSvojePravo", mojId, this.mojaSoba, reff.vrsta);
        global.socket.on("pravo", function (pravo) {
            reff.mojePravo = Number(pravo);
        });
        global.socket.on("ucenjePrekinuto", function (clientName) {
            //alert("Ucenje prekinuto");
            //alert("Prekinuto. Zavrseno: " + reff.ucenjeZavrseno);
            //alert("Zadatak: " + reff.brojZadatka);
            if (reff.brojZadatka == reff.brojSvihZadataka || reff.brojZadatka == (reff.brojSvihZadataka + 1)) {
                return;
            }
            reff.ucenjePrekinuto = true;
            reff.ucenjePrekinuo = clientName;
            var timer = Rx_1.Observable.timer(3000, 1000);
            //alert("Setam timer");
            timer.subscribe(function (t) {
                reff.timer(t);
            });
        });
        global.socket.on("zadatakZadan", function (operandi) {
            reff.operandi = operandi;
            reff.mogucaPredaja = true;
        });
        global.socket.on("zadatakRjesen", function (operandi) {
            reff.operandi = operandi;
            reff.moguceOcijenjivanje = true;
        });
        global.socket.on("zadatakOcijenjen", function (ocijena) {
            //reff.statistika.push(new Statistika(reff.brojZadatka, ocijena));
            if (reff.vrsta == 3) {
                reff.mogucaEvaluacija = true;
            }
            else {
                reff.mogucNastavak = true;
            }
            reff.ocijena = ocijena;
        });
        global.socket.on("zadatakEvaluiran", function (evaluacija) {
            //reff.statistika[reff.brojZadatka-1].evaluacija = evaluacija;
            reff.mogucNastavak = true;
            reff.evaluacija = evaluacija;
        });
        global.socket.on("poruka", function (poruka) {
            reff.poruke.push(poruka);
        });
    }
    Zio1Component.prototype.timer = function (t) {
        if (t == 3) {
            this._router.navigate(['/home']);
        }
    };
    Zio1Component.prototype.zadatakZadan = function () {
        this.zadajPritisnut = true;
        global.socket.emit("zadatakZadan", this.mojaSoba, this.operandi);
    };
    Zio1Component.prototype.zadatakRjesen = function () {
        this.mogucaPredaja = false;
        global.socket.emit("zadatakRjesen", this.mojaSoba, this.operandi);
    };
    Zio1Component.prototype.zadatakOcijenjen = function (ishod) {
        this.moguceOcijenjivanje = false;
        if (this.vrsta == 2) {
            this.mogucNastavak = true;
        }
        global.socket.emit("zadatakOcijenjen", this.mojaSoba, ishod);
    };
    Zio1Component.prototype.zadatakEvaluiran = function (ishod) {
        this.mogucaEvaluacija = false;
        this.mogucNastavak = true;
        global.socket.emit("zadatakEvaluiran", this.mojaSoba, ishod);
    };
    Zio1Component.prototype.nastavak = function () {
        //resetiraj sve
        this.mogucaPredaja = false;
        this.moguceOcijenjivanje = false;
        this.ocijena = undefined;
        this.evaluacija = undefined;
        this.zadajPritisnut = false;
        this.mogucNastavak = false;
        this.mogucaEvaluacija = false;
        this.operandi = [0, 0, 0, 0, 0, 0];
        this.brojZadatka++;
        if (this.brojZadatka == (this.brojSvihZadataka + 1)) {
            this.ucenjeZavrseno = true;
            global.socket.emit("napustanjeUcenja", this.mojaSoba, this.vrsta, undefined);
        }
    };
    Zio1Component.prototype.zavrsi = function () {
        global.socket.emit("otkljucaj");
        this._router.navigate(['/home']);
    };
    Zio1Component.prototype.saljiPoruku = function () {
        if (this.chatText != undefined) {
            if (this.chatText !== "") {
                var name_1 = this._cookieService.get(global.myName);
                var poruka = "[ " + name_1 + " ]: " + this.chatText;
                this.poruke.push(poruka);
                global.socket.emit("poruka", this.mojaSoba, poruka);
                this.chatText = "";
            }
        }
    };
    Zio1Component.prototype.napusti = function () {
        var myName = this._cookieService.get(global.myName);
        global.socket.emit("napustanjeUcenja", this.mojaSoba, this.vrsta, myName);
        if (this.vrsta == 2) {
            this._router.navigate(['/home/dvojke/info']);
        }
        else {
            this._router.navigate(['/home/trojke/info']);
        }
    };
    return Zio1Component;
}());
Zio1Component = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "zio1",
        templateUrl: "Zio1.html"
    }),
    __metadata("design:paramtypes", [core_2.CookieService, router_1.Router])
], Zio1Component);
exports.Zio1Component = Zio1Component;
