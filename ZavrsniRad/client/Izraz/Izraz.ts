export class Izraz{
    first: number;
    second: number;
    result: number;

    constructor(first:number, second: number, result: number){
        this.first = first;
        this.second = second;
        this.result = result;
    }
}