"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Izraz = (function () {
    function Izraz(first, second, result) {
        this.first = first;
        this.second = second;
        this.result = result;
    }
    return Izraz;
}());
exports.Izraz = Izraz;
