"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ZahtjevDvojke = (function () {
    function ZahtjevDvojke(daje, prima) {
        this.daje = daje;
        this.prima = prima;
        this.status = false;
    }
    return ZahtjevDvojke;
}());
exports.ZahtjevDvojke = ZahtjevDvojke;
