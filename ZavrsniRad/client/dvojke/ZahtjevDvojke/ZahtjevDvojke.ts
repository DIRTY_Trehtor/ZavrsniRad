import { Client } from "../../Client/Client";

export class ZahtjevDvojke{

    public daje: Client;
    public prima: Client;
    public status: boolean;

    constructor(daje: Client, prima: Client){
        this.daje = daje;
        this.prima = prima;
        this.status = false; 
    } 

}