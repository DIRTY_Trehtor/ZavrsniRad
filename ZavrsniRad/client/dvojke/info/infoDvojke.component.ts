import { Component } from "@angular/core";
import {Router, Routes} from '@angular/router';
import {CookieService} from 'angular2-cookie/core';

import * as global from "../../Global/Global";
import { Client } from "../../Client/Client";
import { ZahtjevDvojke } from "../ZahtjevDvojke/ZahtjevDvojke";

import * as io from "socket.io-client"

@Component({
    moduleId: module.id,
    selector: "infoDvojke",
    templateUrl: "InfoDvojke.html"
})

export class InfoDvojkeComponent{


    constructor(private _cookieService : CookieService){

    }

}