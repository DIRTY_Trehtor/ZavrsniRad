import { Component, Renderer, ElementRef, ViewChild   } from "@angular/core";
import {Router, Routes} from '@angular/router';
import {CookieService} from 'angular2-cookie/core';

import * as global from "../../Global/Global";
import { Client } from "../../Client/Client";
import { ZahtjevDvojke } from "../ZahtjevDvojke/ZahtjevDvojke";

import * as io from "socket.io-client"

@Component({
    moduleId: module.id,
    selector: "frendDvojke",
    templateUrl: "Frend.html"
})

export class FrendDvojkeComponent{
    usersOnline : Client[];
    odabran: Client = undefined;
    zahtjev: ZahtjevDvojke;
    zahtjevPotdvrdjen: boolean = false;

    constructor(private _router: Router, private _cookieService: CookieService, private _renderer:Renderer){
        let reff = this;
        let id: string = reff._cookieService.get(global.myId);
        let name: string = reff._cookieService.get(global.myName);

        global.socket.emit("getUsersOnline");
        global.socket.on("broadcastUsersOnline", function(usersOnline){
            let pom: Client[] = usersOnline;
            reff.usersOnline = [];
            
            for(let usr of pom){
                if(usr.id !== id){
                    reff.usersOnline.push(usr);
                }
            }
        });
        //neki user se connectao
        global.socket.on("userConnected", function(user){
            reff.usersOnline.push(user);
        });
        global.socket.on("userDisconnected", function(user){
            
            if(reff.odabran.id === user.id){
                reff.odabran = undefined;
            }
            let index;
            for(let el of reff.usersOnline){
                if(user.id === el.id){
                    break;
                }
                index ++;
            }
            reff.usersOnline.splice(index, 1);
        });

        //DOBITAK IMENA SOBE znaci da ste zapocinjete ucenje s prjateljem
        global.socket.on("imeSobeFrend", function(imeSobe , vrsta){
            reff._cookieService.put(global.myRoom, imeSobe);
            reff._cookieService.put(global.myVrsta, vrsta);
            reff.zahtjevPotdvrdjen = true;
        });
        
    }

    zapocni(){
        this._router.navigate(['/home/zio1']);
    }

    odaberi(user: Client){
        if(this.odabran == undefined){
            user.odabran = true;

            this.odabran = user;
        }
        else{
            if(user.odabran == true){
                user.odabran = false;
                this.odabran = undefined;
            }
            else{
                this.odabran.odabran = false;
                user.odabran = true;
                this.odabran = user;
            }
        }
    }
    posaljiZahtjev(){
        let reff = this;

        let name: string = this._cookieService.get(global.myName);
        let age: number = Number(this._cookieService.get(global.myAge));
        let id: string =this._cookieService.get(global.myId);

        let me = new Client(name, age, id, 0);
        this.zahtjev = new ZahtjevDvojke(me, this.odabran);

        global.socket.emit('zahtjevDvojke', reff.zahtjev);

    }
    ponistiZahtjev(){
        let reff = this;
        global.socket.emit('ponistiZahtjevDvojke', reff.zahtjev);
    }
}