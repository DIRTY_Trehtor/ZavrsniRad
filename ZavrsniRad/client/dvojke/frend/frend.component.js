"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var core_2 = require("angular2-cookie/core");
var global = require("../../Global/Global");
var Client_1 = require("../../Client/Client");
var ZahtjevDvojke_1 = require("../ZahtjevDvojke/ZahtjevDvojke");
var FrendDvojkeComponent = (function () {
    function FrendDvojkeComponent(_router, _cookieService, _renderer) {
        this._router = _router;
        this._cookieService = _cookieService;
        this._renderer = _renderer;
        this.odabran = undefined;
        this.zahtjevPotdvrdjen = false;
        var reff = this;
        var id = reff._cookieService.get(global.myId);
        var name = reff._cookieService.get(global.myName);
        global.socket.emit("getUsersOnline");
        global.socket.on("broadcastUsersOnline", function (usersOnline) {
            var pom = usersOnline;
            reff.usersOnline = [];
            for (var _i = 0, pom_1 = pom; _i < pom_1.length; _i++) {
                var usr = pom_1[_i];
                if (usr.id !== id) {
                    reff.usersOnline.push(usr);
                }
            }
        });
        //neki user se connectao
        global.socket.on("userConnected", function (user) {
            reff.usersOnline.push(user);
        });
        global.socket.on("userDisconnected", function (user) {
            if (reff.odabran.id === user.id) {
                reff.odabran = undefined;
            }
            var index;
            for (var _i = 0, _a = reff.usersOnline; _i < _a.length; _i++) {
                var el = _a[_i];
                if (user.id === el.id) {
                    break;
                }
                index++;
            }
            reff.usersOnline.splice(index, 1);
        });
        //DOBITAK IMENA SOBE znaci da ste zapocinjete ucenje s prjateljem
        global.socket.on("imeSobeFrend", function (imeSobe, vrsta) {
            reff._cookieService.put(global.myRoom, imeSobe);
            reff._cookieService.put(global.myVrsta, vrsta);
            reff.zahtjevPotdvrdjen = true;
        });
    }
    FrendDvojkeComponent.prototype.zapocni = function () {
        this._router.navigate(['/home/zio1']);
    };
    FrendDvojkeComponent.prototype.odaberi = function (user) {
        if (this.odabran == undefined) {
            user.odabran = true;
            this.odabran = user;
        }
        else {
            if (user.odabran == true) {
                user.odabran = false;
                this.odabran = undefined;
            }
            else {
                this.odabran.odabran = false;
                user.odabran = true;
                this.odabran = user;
            }
        }
    };
    FrendDvojkeComponent.prototype.posaljiZahtjev = function () {
        var reff = this;
        var name = this._cookieService.get(global.myName);
        var age = Number(this._cookieService.get(global.myAge));
        var id = this._cookieService.get(global.myId);
        var me = new Client_1.Client(name, age, id, 0);
        this.zahtjev = new ZahtjevDvojke_1.ZahtjevDvojke(me, this.odabran);
        global.socket.emit('zahtjevDvojke', reff.zahtjev);
    };
    FrendDvojkeComponent.prototype.ponistiZahtjev = function () {
        var reff = this;
        global.socket.emit('ponistiZahtjevDvojke', reff.zahtjev);
    };
    return FrendDvojkeComponent;
}());
FrendDvojkeComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "frendDvojke",
        templateUrl: "Frend.html"
    }),
    __metadata("design:paramtypes", [router_1.Router, core_2.CookieService, core_1.Renderer])
], FrendDvojkeComponent);
exports.FrendDvojkeComponent = FrendDvojkeComponent;
