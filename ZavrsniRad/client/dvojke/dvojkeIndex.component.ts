import { Component } from "@angular/core";
import {Router, Routes} from '@angular/router';

import * as io from "socket.io-client"
import * as global from "../Global/Global";

@Component({
    moduleId: module.id,
    selector: "dvojkeIndex",
    templateUrl: "DvojkeIndex.html"
})
export class DvojkeIndexComponent{

    brzo: boolean = false;

    constructor(private _router: Router){
        
    }

    brzoUcenje(){
        this.brzo = true;
        this._router.navigate(['/home/dvojke/random']);
        
    }
    sPrijateljem(){
        this.brzo = false;
        this._router.navigate(['/home/dvojke/frend']);

    }

}