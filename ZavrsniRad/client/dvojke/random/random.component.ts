import { Component } from "@angular/core";
import {Router, Routes} from '@angular/router';
import {CookieService} from 'angular2-cookie/core';

import * as global from "../../Global/Global";
import { Client } from "../../Client/Client";

import * as io from "socket.io-client"

@Component({
    moduleId: module.id,
    selector: "randomDvojke",
    templateUrl: "Random.html"
})

export class RandomDvojkeComponent{

    thisUser: Client;
    cekanje: boolean = false;

    constructor(private _cookieService: CookieService, private _router: Router){
        
        let name = this._cookieService.get(global.myName);
        let age = Number(this._cookieService.get(global.myAge));
        let id = this._cookieService.get(global.myId);

        this.thisUser = new Client(name, age, id, 0);

        let reff = this;
        global.socket.on("imeSobe", function(imeSobe, vrsta){
            reff._cookieService.put(global.myRoom, imeSobe);
            reff._cookieService.put(global.myVrsta, vrsta);
            //navigacija
            reff._router.navigate(['/home/zio1']);

        });
    }

    zahtjevZaRandom(){
        global.socket.emit("zahtjevRandomDvojke", this.thisUser);
        this.cekanje = true;
    }
    ponistiZahtjev(){
        global.socket.emit("ponistiZahtjevRandomDvojke", this.thisUser.id);
        this.cekanje = false;
    }

}