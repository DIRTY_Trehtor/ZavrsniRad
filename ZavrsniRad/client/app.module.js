"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var core_2 = require("angular2-cookie/core");
var common_1 = require("@angular/common");
//komponente modula
var app_component_1 = require("./app.component");
var login_component_1 = require("./login/login.component");
var navbar_component_1 = require("./navbar/navbar.component");
var loginNavbar_component_1 = require("./loginNavbar/loginNavbar.component");
var home_component_1 = require("./home/home.component");
var homeInfo_component_1 = require("./homeInfo/homeInfo.component");
var dvojkeIndex_component_1 = require("./dvojke/dvojkeIndex.component");
var random_component_1 = require("./dvojke/random/random.component");
var frend_component_1 = require("./dvojke/frend/frend.component");
var infoDvojke_component_1 = require("./dvojke/info/infoDvojke.component");
var trojkeIndex_component_1 = require("./trojke/trojkeIndex.component");
var infoTrojke_component_1 = require("./trojke/info/infoTrojke.component");
var frend_component_2 = require("./trojke/frend/frend.component");
var random_component_2 = require("./trojke/random/random.component");
var zio1_component_1 = require("./Tecajevi/ZbrajanjeIOduzimanje/zio1.component");
var homeNavigation_component_1 = require("./home/homeNavigation/homeNavigation.component");
//routanje
var app_routing_1 = require("./app.routing");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, router_1.RouterModule, router_1.RouterModule.forRoot(app_routing_1.appRoutes)],
        declarations: [app_component_1.AppComponent,
            login_component_1.LoginComponent, navbar_component_1.NavbarComponent, loginNavbar_component_1.LoginNavbarComponent,
            home_component_1.HomeComponent, homeInfo_component_1.HomeInfoComponent, homeNavigation_component_1.HomeNavigationComponent, zio1_component_1.Zio1Component,
            dvojkeIndex_component_1.DvojkeIndexComponent, random_component_1.RandomDvojkeComponent, frend_component_1.FrendDvojkeComponent, infoDvojke_component_1.InfoDvojkeComponent,
            trojkeIndex_component_1.TrojkeIndexComponent, infoTrojke_component_1.InfoTrojkeComponent, frend_component_2.FrendTrojkeComponent, random_component_2.RandomTrojkeComponent,
        ],
        //pazi na ovo kad stavljas na server
        //, CookieService
        providers: [core_2.CookieService, { provide: common_1.LocationStrategy, useClass: common_1.HashLocationStrategy }],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
