import { Routes, RouterModule } from "@angular/router";

//komponente
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
    import { HomeInfoComponent } from "./homeInfo/homeInfo.component";
    import { DvojkeIndexComponent } from "./dvojke/dvojkeIndex.component";
        import { RandomDvojkeComponent } from "./dvojke/random/random.component";
        import { FrendDvojkeComponent } from "./dvojke/frend/frend.component";
        import { InfoDvojkeComponent } from "./dvojke/info/infoDvojke.component";
    import { TrojkeIndexComponent } from "./trojke/trojkeIndex.component";
        import { InfoTrojkeComponent } from "./trojke/info/infoTrojke.component";
        import { FrendTrojkeComponent } from "./trojke/frend/frend.component";
        import { RandomTrojkeComponent } from "./trojke/random/random.component";
    import { Zio1Component } from "./Tecajevi/ZbrajanjeIOduzimanje/zio1.component";
export const appRoutes: Routes = [
    {path: '', component:LoginComponent },
    {path: 'login', component:LoginComponent },
    {path: 'home', component:HomeComponent , children: [
        {path: '', redirectTo: 'info'},
        {path: 'info', component: HomeInfoComponent},
        {path: 'dvojke', component: DvojkeIndexComponent, children: [
            {path: '', redirectTo:'info'},
            {path: 'info', component: InfoDvojkeComponent},
            {path: 'frend', component: FrendDvojkeComponent},
            {path: 'random', component: RandomDvojkeComponent},
        ]},
        {path: 'trojke', component: TrojkeIndexComponent, children: [
            {path: '', redirectTo: 'info'},
            {path: 'info', component: InfoTrojkeComponent},
            {path: 'frend', component: FrendTrojkeComponent},
            {path: 'random', component: RandomTrojkeComponent},
        ]},
        {path: 'zio1', component: Zio1Component},
    ]},
];