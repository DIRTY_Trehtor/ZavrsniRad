import { Component } from "@angular/core";
import {Router, Routes} from '@angular/router';
import {CookieService} from 'angular2-cookie/core';

import * as global from "../Global/Global";
import * as io from "socket.io-client"

@Component({
    moduleId: module.id,
    selector: "navbar",
    templateUrl: "Navbar.html"
})

export class NavbarComponent {
    
    myName: string = undefined;
    myAge: string = undefined;

    zakljucano: boolean = false;

    constructor(private _cookieService: CookieService, private _router: Router){

        let reff = this;

        this.myName = this._cookieService.get(global.myName);
        this.myAge = this._cookieService.get(global.myAge);

        global.socket.on("zakljucaj", function(drip){
            reff.zakljucano = true;
        });
        global.socket.on("otkljucaj", function(){
            reff.zakljucano = false;
        });
    }

    odjava(){
        global.socket.disconnect();
        this._router.navigate(['login']);
    }
}