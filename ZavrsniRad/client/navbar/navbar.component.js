"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var core_2 = require("angular2-cookie/core");
var global = require("../Global/Global");
var NavbarComponent = (function () {
    function NavbarComponent(_cookieService, _router) {
        this._cookieService = _cookieService;
        this._router = _router;
        this.myName = undefined;
        this.myAge = undefined;
        this.zakljucano = false;
        var reff = this;
        this.myName = this._cookieService.get(global.myName);
        this.myAge = this._cookieService.get(global.myAge);
        global.socket.on("zakljucaj", function (drip) {
            reff.zakljucano = true;
        });
        global.socket.on("otkljucaj", function () {
            reff.zakljucano = false;
        });
    }
    NavbarComponent.prototype.odjava = function () {
        global.socket.disconnect();
        this._router.navigate(['login']);
    };
    return NavbarComponent;
}());
NavbarComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "navbar",
        templateUrl: "Navbar.html"
    }),
    __metadata("design:paramtypes", [core_2.CookieService, router_1.Router])
], NavbarComponent);
exports.NavbarComponent = NavbarComponent;
