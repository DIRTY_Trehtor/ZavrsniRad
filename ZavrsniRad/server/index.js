/*
    Dovlacimo sve MODULE koji nam trebaju
 */
"use strict";
let express = require("express");
var path = require('path');
let chat_app = require('express')();
let http = require('http').Server(chat_app);
let io = require('socket.io')(http);

let usersOnline = [];
let zahtjeviDvojke = [];
let zahtjeviRandomDvojke = [];
let dvojke = [];
let zahtjeviTrojke = [];
let zahtjeviRandomTrojke = [];
let trojke = [];

let brojSobe = 1;


/*
		Ovo tu ustvari nema opce veze za socket.IO
		=> definira parametre podizanja node.js server  .... 
 */
chat_app.use(express.static(__dirname, '/'));
chat_app.use(express.static(__dirname, '/server/'));
chat_app.use(express.static(__dirname + "/..", '/client/'));
chat_app.use(express.static(__dirname + '/node_modules'));

chat_app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){


  socket.on("korisnikPodaci", function(name, age){
      //NOVI USER
      let user = new User(name, age, socket.id);
      usersOnline.push(user);
      console.log("CONNECTION. User: " + user.name + ", age: " + user.age);
      
      //KORISNIK CUVA - salje se tocno ovom korisniku
      socket.emit("userCredentials", user);
      io.emit("userConnected", user);
  });
  

  //DOHVACANJE SVIH KORISNIKA
  socket.on("getUsersOnline", function(){
    socket.emit("broadcastUsersOnline", usersOnline);
  });

  //DOHVACANE ZAHTJEVA za korisnika
  socket.on("getMyRequests", function(id){
    let list = [];
    for(let z of zahtjeviDvojke){
        if(z.prima.id === socket.id){
            list.push(z);
        }
    }
    socket.emit("requestsDvojke", list);
  });

  /*------------------------------------------------------------  ZAJEDNIČKO  ------------------------------------------------------------*/

  socket.on("otkljucaj", function(){
    socket.emit("otkljucaj");
  });

  socket.on("dohvatiSobu", function(soba, vrsta){
    let lista;
    if(vrsta == 2){
      lista = dvojke;
    }
    else{
      lista = trojke;
    }

    let index = undefined;
    let i = 0;
    for(let el of lista){
      if(el.soba === soba){
        index = i;
        break;
      }
      i++;
    }
    if(index == undefined){
      socket.emit("soba", undefined);
    }
    else{
      socket.emit("soba", lista[index]);
    }

  });

  socket.on("poruka", function(poruka ){
      socket.broadcast.emit("porukaSvima", poruka);
    
  });
  socket.on("porukaUcenje", function(soba, poruka){
    socket.broadcast.in(soba).emit("porukeUcenje", poruka);
  });
  socket.on("napustanjeUcenja", function(soba, vrsta, ucenjePrekinuo){
    NapustanjeUcenja(soba, vrsta, ucenjePrekinuo);
  });
  function NapustanjeUcenja(soba, vrsta, ucenjePrekinuo){
    let lista;
    if(vrsta == 2){
       lista = dvojke;
    }
    else{
       lista = trojke;
    }

    let index = 0;
    for(let el of lista){
      if(el.soba === soba){
        
        break;
      }
      index++;
    }
    let element = lista[index];
    if(element == undefined){ //kad zavrsi lekcija, svi pokusavaju prekinuti 
      return;
    }
    lista.splice(index, 1);   //brisemo iz liste dvojki ili trojki

   // if(ucenjePrekinuo != undefined){
      socket.in(soba).emit("ucenjePrekinuto", ucenjePrekinuo);
   //  }
    if(ucenjePrekinuo != undefined)
      io.sockets.in(soba).emit("otkljucaj");

    if(vrsta == 2){
      console.log("PREKIDANJE UCENJA. Clanovi: " + element.prvi.name + ", " + element.drugi.name);
    }
    else{
      console.log("PREKIDANJE UCENJA. Clanovi: " + element.prvi.name + ", " + element.drugi.name+ ", " + element.treci.name);
    }

    let sock;

    //napuštanje soba
    
    sock = io.sockets.connected[element.prvi.id];
    if(sock != undefined){
      sock.leave(soba);
    }
    sock = io.sockets.connected[element.drugi.id];
    if(sock != undefined){
      sock.leave(soba);
    }
    if(vrsta == 3){
      sock = io.sockets.connected[element.treci.id];
      if(sock != undefined){
        sock.leave(soba);
      }
    }
  }

  /*------------------------------------------------------------  U DVOJKAMA  ------------------------------------------------------------*/
  
  //DOHVAT PRAVA
  socket.on("dohvatiSvojePravo", function(id, soba, vrsta){

    //console.log("pravo za: "+ id + ", soba: "+ soba + ", vrsta: "+vrsta);

    let pravo;
    if(vrsta == 2){
      for(let dvojka of dvojke){
        if(dvojka.soba === soba){
          if(dvojka.prvi.id === id){
            pravo = dvojka.prvi.pravo;
            break;
          }
          else if(dvojka.drugi.id === id){
            pravo = dvojka.drugi.pravo;
            break;
          }
        }
      }
    }
    else if(vrsta == 3){
      for(let trojka of trojke){
        
        if(trojka.soba === soba){
         
          if(trojka.prvi.id === id){
            pravo = trojka.prvi.pravo;
            break;
          }
          else if(trojka.drugi.id === id){
            pravo = trojka.drugi.pravo;
            break;
          }
          else if(trojka.treci.id === id){
            pravo = trojka.treci.pravo;
            break;
          }
        }
      }
    }
    socket.emit("pravo", pravo);
  });

  //RANDOM DVOJKE ZAHTJEV
  socket.on("zahtjevRandomDvojke", function(user){
   
    console.log("ZAHTJEV RANDOM DVOJKE. User: "+user.name+", "+user.age);

    if(zahtjeviRandomDvojke.length > 0){
      let rasponGodina = [];
      let partner;

      

      for(let usr of zahtjeviRandomDvojke){
        if(Math.abs(usr.age - user.age) <= 1 && usr.id !== user.id){
          rasponGodina.push(usr);
        }
      }
      let numberOfRaspon = rasponGodina.length;
      if(numberOfRaspon > 0){
        let index = Math.floor(Math.random() * numberOfRaspon);
        partner = rasponGodina[index];
        console.log("DVOJKA STVORENA. Clanovi: " + user.name + ", "+partner.name);

        //treba izbrisati partnera s liste 
        let i = 0;
        for(let usr of zahtjeviRandomDvojke){
          if(partner.id === usr.id){
            index = i;
            break;
          }
          i++;
        }
        zahtjeviRandomDvojke.splice(index, 1);
        
        //stvoriti dvojku
        //stvaramo ROOM
        let imeSobe = "room-" + brojSobe;
        brojSobe++;

        socket.join(imeSobe);
        io.sockets.connected[partner.id].join(imeSobe);

        let nova = new Dvojka(user, partner, imeSobe); 
        let random = Math.floor(Math.random() * 2) + 1;
        if(random == 1){
          nova.prvi.pravo = 1;
          nova.drugi.pravo = 2;
        }
        else{
          nova.prvi.pravo = 2;
          nova.drugi.pravo = 1;
        }

        dvojke.push(nova);

        //obavijestiti partnere

        io.sockets.in(imeSobe).emit("zakljucaj", imeSobe);
         io.sockets.in(imeSobe).emit("imeSobe", imeSobe, 2);
      }
      else{
        zahtjeviRandomDvojke.push(user);
      }
    }
    else{
      zahtjeviRandomDvojke.push(user);
    }

  });
  //PONUŠTI RANDOM DVOJKE ZAHTJEV
  socket.on("ponistiZahtjevRandomDvojke", function(id){

    let index;
    let i = 0;
    for(let el of zahtjeviRandomDvojke){
      if(el.id === id){
        index = i;
        break;
      }
      i++;
    }
    if(index == undefined){
      return;
    }
    zahtjeviRandomDvojke.splice(index, 1);

  });
  //DOBIVAMO ZAHTJEV ZA DVOJKE
  socket.on("zahtjevDvojke", function(zahtjev){
    let pom = new ZahtjevDvojke(zahtjev.daje, zahtjev.prima);
    console.log("ZAHTJEV DVOJKE.  " + pom.daje.name + "->" + pom.prima.name);
    zahtjeviDvojke.push(pom);

    let list = [];
    for(let z of zahtjeviDvojke){
        if(z.prima.id === zahtjev.prima.id){
            list.push(z);
        }
    }
    socket.broadcast.to(zahtjev.prima.id).emit("requestsDvojke", list);
  });

  //POTVRDA ZAHTJEVA DVOJKE
  socket.on("potvrdaZahtjeva", function(zahtjev){
    console.log("POTVRDA DVOJKE. " + zahtjev.prima.name + " potvrđuje "+ zahtjev.daje.name);

    //stvaramo ROOM
    let imeSobe = "room-" + brojSobe;
    socket.join(imeSobe);
    io.sockets.connected[zahtjev.daje.id].join(imeSobe);
    let nova = new Dvojka(zahtjev.daje, zahtjev.prima, imeSobe); 
    let random = Math.floor(Math.random() * 2) + 1;
    if(random == 1){
      nova.prvi.pravo = 1;
      nova.drugi.pravo = 2;
    }
    else{
      nova.prvi.pravo = 2;
      nova.drugi.pravo = 1;
    }
    
    dvojke.push(nova);

    brojSobe++;

    io.to(zahtjev.daje.id).emit("imeSobeFrend", imeSobe, 2);
    io.to(zahtjev.prima.id).emit("imeSobe", imeSobe, 2);
    io.sockets.in(imeSobe).emit("zakljucaj", imeSobe);

    PonistiZahtjevDvojke(zahtjev);

  });

  //UKIDANJE ZAHTJEVA za dvojke
  socket.on("ponistiZahtjevDvojke", function(zahtjev){
    PonistiZahtjevDvojke(zahtjev);
  });
  function PonistiZahtjevDvojke(zahtjev){
      let list = [];
      let index;
      let i = 0;
      for(let z of zahtjeviDvojke){
          if(z.daje.id === zahtjev.daje.id && z.prima.id === zahtjev.prima.id){
              index = i;
              break;
          }
          i++;
      }
      if(index != undefined){
        zahtjeviDvojke.splice(index, 1);
        console.log("UKLANJANJE ZAHTJEV DVOJKE.  "+ zahtjev.daje.name +" -> "+zahtjev.prima.name);


        for(let z of zahtjeviDvojke){
          if(z.prima.id === zahtjev.prima.id){
            list.push(z);
          }
        }
        //console.log("Velicina pronadjenih: "+ list.length);
        io.to(zahtjev.prima.id).emit("requestsDvojke", list);
      }
      
  }

  /* ZADACI DVOJKE */
  socket.on("zadatakZadan", function(soba ,operandi){
    socket.broadcast.to(soba).emit('zadatakZadan', operandi);
  });
  socket.on("zadatakRjesen", function(soba ,operandi){
    socket.broadcast.to(soba).emit('zadatakRjesen', operandi);
  });
  socket.on("zadatakOcijenjen", function(soba, ishod){
    socket.broadcast.to(soba).emit('zadatakOcijenjen', ishod);
  }); 
  socket.on("zadatakEvaluiran", function(soba, ishod){
    socket.broadcast.to(soba).emit('zadatakEvaluiran', ishod);
  });
/*--------------------------------------------------------------------------------------------------------------------------------------*/



/*------------------------------------------------------------  U TROJKAMA  ------------------------------------------------------------*/

  //ZAHTJEV RANDOM TROJKE
  socket.on("zahtjevRandomTrojke", function(user){

    console.log("ZAHTJEV RANDOM TROJKE. User: " + user.name);

      let listaDobrih = [];
      for(let el of zahtjeviRandomTrojke){

          if(Math.abs(el.age - user.age) <= 1 ){
            listaDobrih.push(el);
          }
      }

      let brojDobrih = listaDobrih.length;

      if(brojDobrih >= 2){

        //odabrati 2 random korisnika
        let prvi;
        let drugi;
        console.log("Brojdobrih: " + brojDobrih);
        while(1){
          prvi = Math.floor(Math.random() * (brojDobrih));
          drugi = Math.floor(Math.random() * (brojDobrih));
          console.log("prvi: " + prvi + ", drugi: "+ drugi);
          if(prvi != drugi){
            break;
          }
        }

        let nova = new Trojka(user, listaDobrih[prvi], listaDobrih[drugi]);

        let pravo1;
        let pravo2;
        let pravo3;
      /* while(1){
          pravo1 = Math.floor(Math.random() * 2) + 1;
          pravo2 = Math.floor(Math.random() * 2) + 1;
          pravo3 = Math.floor(Math.random() * 2) + 1;

          if(pravo1 != pravo2 && pravo1 != pravo3 && pravo2 != pravo3){
            break;
          }
        }*/
        nova.prvi.pravo = 1;
        nova.drugi.pravo = 2;
        nova.treci.pravo = 3;

        let imeSobe = "room-" + brojSobe;
        brojSobe++;

        nova.soba = imeSobe;

        trojke.push(nova);

        io.sockets.connected[nova.prvi.id].join(imeSobe);
        io.sockets.connected[nova.drugi.id].join(imeSobe);
        io.sockets.connected[nova.treci.id].join(imeSobe);


        io.sockets.in(imeSobe).emit("zakljucaj", imeSobe);
        io.in(imeSobe).emit("imeSobe", imeSobe, 3);

       // socket.join(imeSobe);
        //io.sockets.connected[partner.id].join(imeSobe);

      }
      else{
        zahtjeviRandomTrojke.push(user);
      }



  });
  //PONUŠTI RANDOM TROJKE ZAHTJEV
  socket.on("ponistiZahtjevRandomTrojke", function(id){

    

    let index;
    let i = 0;
    for(let el of zahtjeviRandomTrojke){
      if(el.id === id){
        index = i;
        break;
      }
      i++;
    }
    if(index == undefined){
      return;
    }
    let pom = zahtjeviRandomTrojke[index];
    console.log("UKIDANJE ZAHTJEV RANDOM TROJKE. User: " + pom.name + ", " + pom.age);
    zahtjeviRandomTrojke.splice(index, 1);

  });

  //DOBIVAMO ZAHTJEV ZA TROJKE
  socket.on("zahtjevTrojke", function(zahtjev){
    let novi = new ZahtjevTrojke(zahtjev.daje, zahtjev.prima1, zahtjev.prima2);
    console.log("ZAHTJEV TROJKE. " + novi.daje.status + " -> " + novi.prima1.status+", "+ novi.prima2.status);
    zahtjeviTrojke.push(novi);

    let list1 = [];
    let list2 = [];
    for(let z of zahtjeviTrojke){
        if(z.prima1.id === zahtjev.prima1.id){
            list1.push(z);
        }
        if(z.prima2.id === zahtjev.prima2.id){
            list2.push(z);
        }
    }

    socket.broadcast.to(zahtjev.prima1.id).emit("requestsTrojke", list1);
    socket.broadcast.to(zahtjev.prima2.id).emit("requestsTrojke", list2);

    //ispisiZahtjeveTrojke();

  });

  //DOHVACANJE ZAHTJEVA TROJKI KORISNIKA
  socket.on("getMyRequestsTrojke", function(id){
    let list = [];
    for(let z of zahtjeviTrojke){
        if(z.prima1.id === id || z.prima2.id === id){
            list.push(z);
        }
    }
    socket.emit("requestsTrojke", list);
  });

  //POTVRDA ZAHTJEVA TROJKI // radi i za ukidanje potvrde
  socket.on("potvrdaZahtjevaTrojke", function(zahtjev, od){
    PotvrdaZahtjevaTrojke(zahtjev, od);
  });
  function PotvrdaZahtjevaTrojke(zahtjev, od){
    
    //ispisiZahtjeveTrojke();

    let z;
    for(let el of zahtjeviTrojke){
        //console.log("Petlja, daje id: "+ el.daje.id);
        if(el.daje.id === zahtjev.daje.id){
            z = el;
            break;
        }
    }
    if(z == undefined)
      return;

    if(od == 1){
      z.prima1.status = zahtjev.prima1.status;
    }
    else{
      z.prima2.status = zahtjev.prima2.status;
    }
    
    console.log("MANIPULACIJA ZAHTJEVOM. "+ z.daje.name+"->"+z.prima1.name+","+z.prima2.name+". Status1: "+ z.prima1.status+", Status2: "+ z.prima2.status);

    socket.broadcast.to(zahtjev.daje.id).emit("updateZahtjevTrojke", z);
    if(z.prima1.status == true){ //ne želimo spamati nekog ako nije prihvatio
      socket.broadcast.to(zahtjev.prima1.id).emit("updateZahtjevTrojke", z);
    }
    if(z.prima2.status == true){ //ne želimo spamati nekog ako nije prihvatio
      socket.broadcast.to(zahtjev.prima2.id).emit("updateZahtjevTrojke", z);
    }

    let list1 = [];
    let list2 = [];
    for(let el of zahtjeviTrojke){
        if(el.prima1.id === z.prima1.id){
            list1.push(el);
        }
        if(el.prima2.id === z.prima2.id){
              list2.push(el);
          }
    }

    socket.broadcast.to(z.prima1.id).emit("requestsTrojke", list1);
    socket.broadcast.to(z.prima2.id).emit("requestsTrojke", list2);

    //ako su SVI prihvatili
    if(z.prima1.status == true && z.prima2.status == true){
      
      let imeSobe = "room-" + brojSobe;
      brojSobe++;

      io.sockets.connected[z.daje.id].join(imeSobe);
      io.sockets.connected[z.prima1.id].join(imeSobe);
      io.sockets.connected[z.prima2.id].join(imeSobe);

      z.daje.pravo = 1;
      z.prima1.pravo = 2;
      z.prima2.pravo = 3;
      
      let nova = new Trojka(z.daje, z.prima1, z.prima2, imeSobe);
      trojke.push(nova);

      console.log(z.daje.name + ", " + z.prima1.name + " i " + z.prima2.name + " su u TROJCI.");

      io.sockets.in(imeSobe).emit("imeSobe", imeSobe, 3);
      io.sockets.in(imeSobe).emit("zakljucaj", imeSobe);
    }

  }
//UKIDANJE ZAHTJEVA TROJKE
socket.on("ponistiZahtjevTrojke", function(id){

  PonistiZahtjevTrojke(id);

});

function PonistiZahtjevTrojke(id){
  let index;
  let i = 0;
  for(let el of zahtjeviTrojke){
    if(el.daje.id === id){
      index = i;
      break;
    }
    i++;
  }
  if(index == undefined){
    return;
  }
  let pom = zahtjeviTrojke[index];
  zahtjeviTrojke.splice(index, 1);
  console.log("UKLANJANJE ZAHTJEV TROJKE. "+pom.daje.name + " -> "+ pom.prima1.name + ", " + pom.prima2.name);
  


  let list1 = [];
  let list2 = [];
  for(let el of zahtjeviTrojke){

    if(el.daje.id !== id){
      if(el.prima1.id === pom.prima1.id){
          list1.push(el);
      }
      if(el.prima2.id === pom.prima2.id){
          list2.push(el);
      }
    }
  }

  socket.broadcast.to(pom.prima1.id).emit("zahtjevTrojkeUklonjen");
  socket.broadcast.to(pom.prima2.id).emit("zahtjevTrojkeUklonjen");

  socket.broadcast.to(pom.prima1.id).emit("requestsTrojke", list1);
  socket.broadcast.to(pom.prima2.id).emit("requestsTrojke", list2);

  //ispisiZahtjeveTrojke();
}

function ispisiZahtjeveTrojke(){

    console.log("Zahtjevi trojke: ("+zahtjeviTrojke.length+")");
    for(let z of zahtjeviTrojke){
      console.log("\t\t"+z.daje.name+" -> "+z.prima1.name + ", " + z.prima2.name);
    }

}

/*--------------------------------------------------------------------------------------------------------------------------------------*/
  //disconect
  socket.on('disconnect', function(){
    let userIndex;
    for(var i = 0; i < usersOnline.length; i++){
        if(usersOnline[i].id === socket.id){
          userIndex = i;
          break;
        }
    }

    if(userIndex != undefined){
      let user = usersOnline[userIndex];

      closeUserDependencies(user.id);

      usersOnline.splice(userIndex, 1);
      io.emit("userDisconnected", user);
      console.log("DISCONNECTING. User: "+user.name+", age: "+user.age);
    }
  });

  function closeUserDependencies(userId){
    //zahtjevi trojke

    console.log("Zatvaramo sve korisnikove veze");

    let ovlast;
    for(let zt of zahtjeviTrojke){
      if(zt.daje.id === userId){
        ovlast = 1;
        //
      }
      else if(zt.prima1.id === userId && zt.prima1.status == true){
        ovlast = 2;
        zt.prima1.status = false;
        break;
         // 
      }
      else if(zt.prima2.id === userId && zt.prima2.status == true){
        ovlast = 3;
        zt.prima2.status = false;
        break;
        //io.sockets.emit("potvrdaZahtjevaTrojke", zt, 2);
        
      }
    }
    console.log("Ovlast: " + ovlast);
    if(ovlast != undefined){
      if(ovlast == 1){
        //io.sockets.emit("ponistiZahtjevTrojke", userId);
        PonistiZahtjevTrojke(userId);
      }
      else if(ovlast == 2){
        PotvrdaZahtjevaTrojke(zahtjeviTrojke[index], 1);
      }
      else if(ovlast == 3){
        PotvrdaZahtjevaTrojke(zahtjeviTrojke[index], 2);
      }
    }
    //zahtjevi dvojke
    let index = undefined;
    let i = 0;
    for(let zd of zahtjeviDvojke){
      if(zd.daje.id === userId){
        index = i;
        break;
      }
      i++;
    }
    if(index != undefined){
      PonistiZahtjevDvojke(zahtjeviDvojke[index]);
    }
    //dvojke
    for(let d of dvojke){
      if(d.prvi.id === userId || d.drugi.id === userId){
        let name;
        if(d.prvi.id === userId){
          name = d.prvi.name;
        }
        if(d.drugi.id === userId){
          name = d.drugi.name;
        }
        //io.sockets.emit("napustanjeUcenja", d.soba, 2, name);
        NapustanjeUcenja(d.soba, 2, name);
        break;
        //socket.on("napustanjeUcenja", function(soba, vrsta, ucenjePrekinuo){
      }
    }

    //trojke
    index = 0;
    for(let t of trojke){
      if(t.prvi.id === userId || t.drugi.id === userId || t.treci.id === userId){
        let name;
        if(t.prvi.id === userId){
          name = t.prvi.name;
        }
        if(t.drugi.id === userId){
          name = t.drugi.name;
        }
        if(t.treci.id === userId){
          name = t.treci.name;
        }
        //io.sockets.emit("napustanjeUcenja", d.soba, 3, name);
        NapustanjeUcenja(t.soba, 3, name);
        break;
        //socket.on("napustanjeUcenja", function(soba, vrsta, ucenjePrekinuo){
      }
    }
    //zahtjeviRandom dvojke
    index = undefined;
    i = 0;
    for(let rd of zahtjeviRandomDvojke){
      if(rd.id === userId){
        index = i;
        break;
      }
      i++;
    }
    if(index != undefined){
      zahtjeviRandomDvojke.splice(index, 1);
    }
    //zahtjeviRandom trojke
    index = undefined;
    i = 0;
    for(let rd of zahtjeviRandomTrojke){
      if(rd.id === userId){
        index = i;
        break;
      }
      i++;
    }
    if(index != undefined){
      zahtjeviRandomTrojke.splice(index, 1);
    }

}

});

http.listen(3000, function(){
  console.log('listening on *:3000');
});


/* ------------------ STRUKTURE PODATAKA --------------------*/ 
function User(name, age, id, pravo, status){
  this.name = name;
  this.age = age;
  this.id = id;
  this.pravo = pravo;
  this.status = status;
  /*
    a: 1-rjesava, 2-zadaje, 3-kontrolira
   */
}
function ZahtjevDvojke(daje, prima){
  this.daje = daje;
  this.prima = prima;
}
function ZahtjevTrojke(daje, prima1, prima2){
  this.daje = daje;
  this.prima1 = prima1;
  this.prima2 = prima2;
}

function Dvojka(prvi, drugi, soba){
    this.prvi = prvi;
    this.drugi = drugi;
    this.soba = soba;
}
function Trojka(prvi, drugi, treci, soba){
  this.prvi = prvi;
  this.drugi = drugi;
  this.treci = treci;
  this.soba = soba;
}
